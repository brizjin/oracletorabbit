﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    [OracleCustomTypeMapping("SYS.ANYDATA")]  
    class anydata : INullable, IOracleCustomType, IOracleCustomTypeFactory
    {
        public anydata()
        {
        }

        public bool IsNull { get; set; }

        public void FromCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt)
        {
            //OracleUdt.SetValue(con, pUdt, "C_VALUE", C_VALUE);
        }

        public void ToCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt)
        {
            //C_VALUE = (OracleClob)OracleUdt.GetValue(con, pUdt, "C_VALUE");
        }

        public IOracleCustomType CreateObject()
        {
            return new anydata();
        }

        
    }
}
