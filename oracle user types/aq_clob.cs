﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    [OracleCustomTypeMapping("IBS.AQ_CLOB")]  
    class aq_clob : INullable, IOracleCustomType, IOracleCustomTypeFactory
    {
        public aq_clob()
        {
        }

        public aq_clob(OracleConnection con, string value)
        {
            this.C_VALUE = new OracleClob(con);
            this.value = value;
        }

        public bool IsNull { get; set; }

        public void FromCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "C_VALUE", C_VALUE);
        }

        public void ToCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt)
        {
            C_VALUE = (OracleClob)OracleUdt.GetValue(con, pUdt, "C_VALUE");
        }

        [OracleObjectMappingAttribute("C_VALUE")]
        public OracleClob C_VALUE { get; set; }

        public string value
        {
            get
            {
                return this.C_VALUE.Value;
            }
            set
            {
                this.C_VALUE.Erase();
                byte[] arr = value.GetBytes();
                this.C_VALUE.Write(arr, 0, arr.Length);
            }
        }

        public IOracleCustomType CreateObject()
        {
            return new aq_clob();
        }

        public override string ToString()
        {
            return this.value;
        }
        
    }
}
