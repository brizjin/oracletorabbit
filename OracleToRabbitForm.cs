﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RabbitMQ.Client;

namespace WindowsFormsApplication1
{
    public partial class OracleToRabbitForm : Form
    {
        oradb db = new oradb();

        public OracleToRabbitForm()
        {
            InitializeComponent();
        }

        private void OracleToRabbitForm_Load(object sender, EventArgs e)
        {
            waitingForm wf = new waitingForm();
            BackgroundWorker wrk = new BackgroundWorker();
            wrk.DoWork += delegate (object senderW, DoWorkEventArgs eW)
            {
                try
                {
                    db.openConnectionByTNS("tnsnames.ora", "MSB", "portal2", "abc123");
                }
                catch (Oracle.DataAccess.Client.OracleException ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    eW.Result = 0;
                }

            };
            wrk.RunWorkerCompleted += delegate (object senderW, RunWorkerCompletedEventArgs eW)
            {
                wf.Hide();
                if (eW.Result != null)//Если не удалось установить соединение
                {
                    this.Close();
                }
                this.connect_to_aq();

            };
            wrk.RunWorkerAsync();
            wf.ShowDialog();
        }

        //1. Получить список всех очередей
        /*private void button1_Click_1(object sender, EventArgs e)
        {
            this.comboBox1.DataSource = db.select("select t.* from ALL_QUEUES t");//ALL_QUEUES //ALL_QUEUE_TABLES
            this.comboBox1.DisplayMember = "name";
            this.comboBox1.ValueMember = "name";
            this.comboBox1.SelectedValue = "ISIMPLE_OUT";
            //
        }

        //2. Получить содержание очереди Oracle
        private void button2_Click(object sender, EventArgs e)
        {
            string sql = string.Format("select * from IBS.{0}_TBL t", this.comboBox1.SelectedValue);
            this.dataGridView1.DataSource = db.select(sql);
            this.dataGridView1.Columns.Remove("MSGID");
        }

        //3. Отправить сообщение в очередь
        private void button3_Click(object sender, EventArgs e)
        {
            string qname = this.comboBox1.SelectedValue.ToString();
            string message = System.IO.File.ReadAllText("put_str_example.txt");
            db.enqueue(qname, message, qname);            
        }

        //4. Получить соообщение из очереди Oracle
        private void button4_Click(object sender, EventArgs e)
        {
            string qname = this.comboBox1.SelectedValue.ToString();
            this.richTextBox1.Text = db.dequeue(qname, qname);
        }

        //5.Очистить таблицу очереди Oracle. не доступно для пользователя portal2
        private void button5_Click(object sender, EventArgs e)
        {
            string qtname = this.comboBox1.SelectedValue.ToString() + "_TBL";
            db.purge_queue_table(qtname);
        }

        //6. На событие "появилось сообщение" добавляется обработчик запись в лог
        private void button6_Click(object sender, EventArgs e)
        {
            string qname = this.comboBox1.SelectedValue.ToString();
            db.message_available_event(qname,delegate (object qsender, OracleAQMessageAvailableEventArgs qe)
            {
                //this.richTextBox1.Text = qe.AvailableMessages.ToString();
                Console.WriteLine("msg available " + System.DateTime.Now + " " + oradb.ByteArrayToString(qe.MessageId[0]) + " " + qe.AvailableMessages.ToString());//Encoding.UTF8.GetString(e.MessageId[0], 0, 32));
            });
        }*/


        /*IModel model;
        string exchangeName = "Ex";
        string queueName = "Qu";
        string routingKey = "K";

        //Подключимся к Rabbit
        private void button7_Click(object sender, EventArgs e)
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = "amqp://guest:guest@127.0.0.1:5672/";
            IConnection conn = factory.CreateConnection();

            model = conn.CreateModel();

           

            model.ExchangeDeclare(exchangeName, ExchangeType.Direct);
            model.QueueDeclare(queueName, false, false, false, null);
            model.QueueBind(queueName, exchangeName, routingKey, null);
        }

        //Тест публикации в Rabbit
        private void button8_Click(object sender, EventArgs e)
        {
            byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes("Hello, world!");
            model.BasicPublish(exchangeName, routingKey, null, messageBodyBytes);
        }

        delegate void p();*/

        //Опубликовывать в Rabbit сообщения появившиеся в очереди Oracle
        /*private void button5_Click_1(object sender, EventArgs e)
        {
            string qname = this.comboBox1.SelectedValue.ToString();
            db.message_available_event(qname, delegate (object qsender, OracleAQMessageAvailableEventArgs qe)
            {
                //byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes(db.dequeue(qname, qname) + "3");
                //model.BasicPublish(exchangeName, routingKey, null, messageBodyBytes);
                //Delegate  = delegate {
                    //this.richTextBox1.Text = db.dequeue(qname, qname);
                 //   ;
                //};
                this.Invoke(new p(delegate {
                    this.richTextBox1.Text = db.dequeue(qname, qname);
                }));
                
            });
        }*/

        /*private void button10_Click(object sender, EventArgs e)
        {
            string qname = this.comboBox1.SelectedValue.ToString();
            //db.dequeue(qname, qname);
            db.purge_queue_table(qname);
        }*/
        private void add_message(string message)
        {
            System.Windows.Forms.RichTextBox rt = new System.Windows.Forms.RichTextBox();

            rt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            rt.Location = new System.Drawing.Point(0, 0);
            rt.Name = "richTextBox1";
            rt.Size = new System.Drawing.Size(633, 372);
            rt.TabIndex = 5;
            rt.Text = message;

            TabPage tab = new TabPage(System.DateTime.Now.ToLongTimeString());
            this.tabControl1.TabPages.Insert(0, tab);
            this.tabControl1.SelectedTab = tab;

            tab.Controls.Add(rt);
        }

        delegate void p();
        private void connect_to_aq()
        {
            string qname = "ISIMPLE_OUT";
            string consumer = "ISIMPLE_AG";
            db.message_available_event(qname, consumer, delegate (object qsender, OracleAQMessageAvailableEventArgs qe)
            {
                this.Invoke(new p(delegate {
                    this.add_message(db.dequeue(qname, consumer));
                }));
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string qname = "ISIMPLE_OUT";
            string recipient = "ISIMPLE_AG";
            string message = System.IO.File.ReadAllText("put_str_example.txt");
            db.enqueue(qname, message, recipient);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string qname = "ISIMPLE_OUT";
            string consumer = "ISIMPLE_AG";
            this.add_message(db.dequeue(qname, consumer));
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.tabControl1.TabPages.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.add_message(db.xml_request2("<GET_ARC_MOVE>hello</GET_ARC_MOVE>"));
        }
    }
}
