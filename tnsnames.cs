﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public class tnsnames
    {
        //public string str;
        public Dictionary<string, string> dict = new Dictionary<string, string>();

        public tnsnames(string file_name)
        {
            string file_content = System.IO.File.ReadAllText(file_name);
            
            //Удаляем все комментарии начинающиеся с #
            //И все пустые строки
            file_content = Regex.Replace(file_content, @"(^#(.)+$[\r\n]*)|(^\s*$[\r\n]*)", "", RegexOptions.Multiline).TrimEnd();

            using (StringReader reader = new StringReader(file_content))
            {
                string prevline = null;
                string nextline = null;
                file_content = "";

                while (!String.IsNullOrEmpty(nextline = reader.ReadLine()))
                {
                    if (prevline == null) ;
                    else if (nextline[0] == ' ' || nextline[0] == '	')
                        file_content += prevline.Replace("\r\n", "");
                    else
                        file_content += prevline + "\r\n";
                    prevline = nextline;
                }
                file_content += prevline;
            }
            //Удаляем все пробелы
            file_content = Regex.Replace(file_content, @"[ |	]+", "", RegexOptions.Multiline).TrimEnd();

            using (StringReader reader = new StringReader(file_content))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] arr = line.Split(new char[] { '=' }, 2);
                    this.dict.Add(arr[0], arr[1]);
                }
            }
        }

        public string this[string dbname]
        {
            get
            {
                return this.dict[dbname];
            }
        }

          /*string pattern =
        @"(?<name>[A-Z\-_]+\.WORLD)|" +
        @"(PROTOCOL = (?<protocol>[A-Z]+))|" +
        @"(HOST = (?<host>[A-Z0-9._]+))|" +
        @"(PORT = (?<port>[0-9]+))|" +
        @"(SID = (?<sid>[A-Za-z0-9.\-_]+))|" +
        @"(SERVICE_NAME = (?<service>[A-Z\-._]+))";

      TNSEntry entry = null;

      foreach (Match m in Regex.Matches(text, pattern))
      {

        string name = m.Groups["name"].Value;
        string protocol = m.Groups["protocol"].Value;
        string host = m.Groups["host"].Value;
        string port = m.Groups["port"].Value;
        string service = m.Groups["service"].Value;
        string sid = m.Groups["sid"].Value;*/
    }
}
