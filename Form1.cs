﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        /*string cnnStr = @"User Id=ibs;
                          Password=ibs;
                          Data Source=(DESCRIPTION=(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(Host = 10.104.18.163 )(Port = 1521)))(CONNECT_DATA = 
                          (SERVICE_NAME = cftdev)));";*/

        oradb db = new oradb();

        BackgroundWorker wrk = new BackgroundWorker();

        public Form1()
        {
            InitializeComponent();

            waitingForm wf = new waitingForm();

            wrk.DoWork += delegate
            {
                //db.openConnection(cnnStr);
                //db.openConnection("ibs", "ibs", "10.104.18.146", "cftdev");
                db.openConnectionByTNS("tnsnames.ora", "MSB","ibs", "HtuRhtl");
            };
            wrk.RunWorkerCompleted += delegate
            {
                //this.Text = "Соединение установлено";
                wf.Hide();
            };
            wrk.RunWorkerAsync();
            wf.ShowDialog();

            string test_put = System.IO.File.ReadAllText("put_str_example.txt");
            this.richTextBox1.Text = test_put;
            this.richTextBoxEnqueue.Text = test_put;

            
        }

       



        private void button1_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = typeof(DataTable);
            //this.dataGridView1.DataSource = db.select("select utl_raw.cast_to_varchar2(t.msgid),t.enq_time,t.user_data.C_VALUE from SAFO_OUT_TBL t");
            this.dataGridView1.DataSource = db.select("select * from ibs.SAFO_OUT_TBL t");
            
            this.dataGridView1.Columns.Remove("MSGID");
            /*DataGridViewTextBoxColumn c = new DataGridViewTextBoxColumn();
            c.Name = "MSGID";
            c.HeaderText = "MSGID";
            c.DataPropertyName = "MSGID";
            this.dataGridView1.Columns.Insert(1,c);

            this.dataGridView1.CellFormatting += delegate(object sender2, DataGridViewCellFormattingEventArgs e2)
            {
                if (e2.ColumnIndex != 1 || e2.Value == null) return;
                byte[] array = (byte[])e2.Value;
                e2.Value = string.Empty;
                foreach (byte b in array)
                {
                    e2.Value += String.Format("{0:x2}", b);
                    //e2.Value += b.ToString();
                }
            };*/

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = typeof(DataTable);
            //this.dataGridView1.DataSource = db.select("select utl_raw.cast_to_varchar2(t.msgid),t.enq_time,t.user_data.C_VALUE from SAFO_IN_TBL t");
            this.dataGridView1.DataSource = db.select("select * from ibs.SAFO_IN_TBL t");
            this.dataGridView1.Columns.Remove("MSGID");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Text = db.get2clob("SAFO");
        }


        private void button6_Click(object sender, EventArgs e)
        {
            db.purge_queue("SAFO");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            db.message_available();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            db.enqueue("SAFO_OUT", this.richTextBoxEnqueue.Text, "SAFO_AG");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.richTextBoxEnqueue.Text = db.dequeue("SAFO_OUT", "SAFO_AG");
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            var corr_id = db.put2clob("SAFO", /*System.DateTime.Now + */this.richTextBox1.Text);
            string s = db.get2clob("SAFO", corr_id);
            Console.WriteLine(s);

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            db.enqueue(this.richTextBoxEnqueue.Text);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            db.enqueue("SAFO_IN", this.richTextBoxEnqueue.Text, "SAFO_AG");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string xml_in = this.richTextBox1.Text;
            xml_in = System.IO.File.ReadAllText("rez_request.txt");
            this.richTextBox1.Text = db.xml_request(xml_in);
        }

        /*private void button10_Click(object sender, EventArgs e)
        {
            tnsnames tns = new tnsnames("tnsnames.ora");
            this.richTextBoxEnqueue.Text = tns.GetStr();
        }*/

    }


}
