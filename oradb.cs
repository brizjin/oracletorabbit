﻿using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class oradb
    {
        //string cnnStr;// = "User Id=ibs;Password=ibs;Data Source=(DESCRIPTION=(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(Host = 10.104.18.163 )(Port = 1521)))(CONNECT_DATA = (SERVICE_NAME = cfttest)));";
        OracleConnection cnn;
        public System.Collections.Generic.Dictionary<string, OracleAQQueue> qdict;

        public oradb()
        {
            qdict = new Dictionary<string, OracleAQQueue>();
        }

        public void openConnection(string CnnString)
        {
            
            cnn = new OracleConnection(CnnString);            
            cnn.Open();            
        }
        public void openConnectionByTNS(string TnsFileName, string DbName,string user,string pasw)
        {
            string cnnStr = @"User Id={0};
                              Password={1};
                              Data Source={2};";
            cnn = new OracleConnection(string.Format(cnnStr,user,pasw,new tnsnames(TnsFileName)[DbName]));
            cnn.Open();
        }

        public void openConnection(string user,string pasw, string host, string service_name)
        {            
            string cnnStr = @"User Id={0};
                            Password={1};
                            Data Source=(DESCRIPTION=(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)
                            (Host = {2})(Port = 1521)))(CONNECT_DATA = 
                            (SERVICE_NAME = {3})));";
            cnnStr = string.Format(cnnStr, user, pasw, host, service_name);
            cnn = new OracleConnection(cnnStr);
            cnn.Open();
        }

        public DataTable select(string sqlQuery)
        {
            OracleCommand cmd = new OracleCommand(sqlQuery,cnn);
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Load(cmd.ExecuteReader());
            return dt;
        }

        public string get2clob(string queueName)
        {
            OracleCommand cmd = cnn.CreateCommand();
            cmd.CommandText = "ibs.Z$CIT_BO_EXT_CALL_LIB.GET2CLOB";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //cmd.Parameters.Add("retVal", OracleDbType.Raw, 32000, System.Data.ParameterDirection.ReturnValue);
            cmd.Parameters.Add("retVal", OracleDbType.Raw, 32, null, System.Data.ParameterDirection.ReturnValue);
            cmd.Parameters.Add("p_source_code", OracleDbType.Varchar2, 32, queueName, ParameterDirection.InputOutput);
            cmd.Parameters.Add("p_mess_body", OracleDbType.Clob, null, System.Data.ParameterDirection.Output);
            cmd.Parameters.Add("p_corr", OracleDbType.Varchar2, 32, null, System.Data.ParameterDirection.InputOutput);
            
            cmd.ExecuteScalar();

            Oracle.DataAccess.Types.OracleClob msg_body = (Oracle.DataAccess.Types.OracleClob)cmd.Parameters["p_mess_body"].Value;

            string ret = "";

            if (msg_body.Length > 0)
                ret = msg_body.Value;

            return ret;
            //System.Windows.Forms.MessageBox.Show(clob.Value);
        }

        public void put2str(string queueName,string msg)
        {
            OracleCommand cmd = cnn.CreateCommand();
            cmd.CommandText = "ibs.Z$CIT_BO_EXT_CALL_LIB.PUT2STR";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add("retVal", OracleDbType.Raw, 32, null, System.Data.ParameterDirection.ReturnValue);
            cmd.Parameters.Add("p_source_code", OracleDbType.Varchar2, 32, queueName, ParameterDirection.Input);
            cmd.Parameters.Add("p_mess_body", OracleDbType.Varchar2, 32000, msg, System.Data.ParameterDirection.InputOutput);
            cmd.ExecuteScalar();
        }

        public string put2clob(string queueName, string msg)
        {
            OracleCommand cmd = cnn.CreateCommand();
            cmd.CommandText = "Z$CIT_BO_EXT_CALL_LIB.PUT2CLOB";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add("retVal", OracleDbType.Varchar2, 32, null, System.Data.ParameterDirection.ReturnValue);
            cmd.Parameters.Add("p_source_code", OracleDbType.Varchar2, 32, queueName, ParameterDirection.Input);
            cmd.Parameters.Add("p_mess_body", OracleDbType.Clob, msg, System.Data.ParameterDirection.InputOutput);

            cmd.ExecuteScalar();

            return cmd.Parameters["retVal"].Value.ToString();
        }
        public string get2clob(string queueName, string corr_id)
        {
            string msg = null;
            OracleCommand cmd = cnn.CreateCommand();
            cmd.CommandText = "ibs.Z$CIT_BO_EXT_CALL_LIB.GET2CLOB";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.Add("retVal", OracleDbType.Varchar2, 32, null, System.Data.ParameterDirection.ReturnValue);
            cmd.Parameters.Add("p_source_code", OracleDbType.Varchar2, 32, queueName, ParameterDirection.Input);
            cmd.Parameters.Add("p_mess_body", OracleDbType.Clob, msg, System.Data.ParameterDirection.Output);
            cmd.Parameters.Add("corr_id", OracleDbType.Varchar2, 32, corr_id, System.Data.ParameterDirection.InputOutput);
            cmd.ExecuteScalar();
            //Oracle.DataAccess.Types.OracleBinary[] retVal = (Oracle.DataAccess.Types.OracleBinary[])cmd.Parameters["retVal"].Value;
            Oracle.DataAccess.Types.OracleClob c = (Oracle.DataAccess.Types.OracleClob)cmd.Parameters["p_mess_body"].Value;
            return c.Value.ToString();
        }

        public void enqueue(string msg)
        {
            OracleCommand cmd = cnn.CreateCommand();

            //cmd.CommandText = "Z$RUNTIME_AQ_LIB.INIT_PUT";
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            

            string plsqltext = @"declare
                                    msg_id varchar2(32);
                                 begin
                                    ibs.Z$RUNTIME_AQ_LIB.INIT_PUT(null
                                                           ,true --Доступно после commit
                                                           ,null --процедура трансформации типов
                                                           ,1    --приоритет
                                                           ,0    --время в секундах до доступности записи
                                                           ,999999 --время в секундах до потухания
                                                           ,null   --не используется
                                                           --,Z$CIT_BO_ILIB.OUR_SYS.A#CODE||'_AG;;'
                                                           ,'SAFO_AG;'
                                                           );
                           
	                                msg_id := ibs.Z$RUNTIME_AQ_LIB.PUT_CLOB('SAFO_OUT'
                                                                       ,null
                                                                       ,:body);
                                end;
                                ";
            cmd.CommandText = plsqltext;
            cmd.Parameters.Add("body", OracleDbType.Clob, msg, ParameterDirection.Input);
            cmd.ExecuteNonQuery();
        }
        public void purge_queue(string queueName)
        {
            OracleCommand cmd = cnn.CreateCommand();
            string plsqltext = @"begin
                                    DBMS_aqadm.purge_queue_table(:queue_name || '_IN_TBL' ,null,null);
                                    DBMS_aqadm.purge_queue_table(:queue_name || '_OUT_TBL',null,null);
                                 end;
                                ";
            cmd.CommandText = plsqltext;
            cmd.Parameters.Add("queue_name", OracleDbType.Varchar2, queueName, ParameterDirection.Input);
            cmd.ExecuteNonQuery();
        }
        public void purge_queue_table(string queueTableName)
        {
            OracleCommand cmd = cnn.CreateCommand();
            string plsqltext = @"begin
                                    DBMS_aqadm.purge_queue_table(:queue_name ,null,null);
                                 end;
                                ";
            cmd.CommandText = plsqltext;
            cmd.Parameters.Add("queue_name", OracleDbType.Varchar2, "IBS." + queueTableName, ParameterDirection.Input);
            cmd.ExecuteNonQuery();
        }




        public void message_available()
        {
            OracleAQQueue q = new OracleAQQueue("IBS.SAFO_OUT",cnn, OracleAQMessageType.Xml);
            qdict.Add("SAFO_OUT", q);
            q.NotificationConsumers = new string[1]{"SAFO_AG"};
            q.MessageAvailable += delegate(object sender, OracleAQMessageAvailableEventArgs e)
            {
                Console.WriteLine("msg available " + System.DateTime.Now + " " + ByteArrayToString(e.MessageId[0]));//Encoding.UTF8.GetString(e.MessageId[0], 0, 32));

            };
        }

        int msg_av_counter = 1;
        public void message_available_event(string queue_name,string consumer_name,OracleAQMessageAvailableEventHandler d)
        {
            OracleAQQueue q = new OracleAQQueue("IBS." + queue_name, cnn, OracleAQMessageType.Xml);
            qdict.Add(queue_name + msg_av_counter, q);
            msg_av_counter += 1;
            q.NotificationConsumers = new string[1] { consumer_name };
            q.MessageAvailable += d;
        }


        public void enqueue(string QueueName, string msg, string recipient)
        {
            OracleTransaction txn = cnn.BeginTransaction();
            OracleAQQueue queue = new OracleAQQueue("IBS." + QueueName, cnn, OracleAQMessageType.Udt, "IBS.AQ_CLOB");
            OracleAQMessage enqMsg = new OracleAQMessage(new aq_clob(cnn,msg));
            
            // Prepare to Enqueue
            queue.EnqueueOptions.Visibility = OracleAQVisibilityMode.OnCommit;
            queue.EnqueueOptions.DeliveryMode = OracleAQMessageDeliveryMode.Persistent;
            
            //enqMsg.Recipients = new OracleAQAgent[]{new OracleAQAgent("SAFO_AG")};
            enqMsg.Recipients = new OracleAQAgent[] { new OracleAQAgent(recipient) };
            enqMsg.Priority = 1;
            enqMsg.Expiration = 999999;

            // Enqueue message
            queue.Enqueue(enqMsg);
            txn.Commit();
        }

        public string dequeue(string QueueName, string consumer)
        {
            

            //OracleTransaction txn = cnn.BeginTransaction();
            OracleAQQueue queue = new OracleAQQueue("IBS." + QueueName, cnn, OracleAQMessageType.Udt, "IBS.AQ_CLOB");
            
            OracleAQDequeueOptions optitons = new OracleAQDequeueOptions();
            optitons.Visibility = OracleAQVisibilityMode.Immediate;
            optitons.ConsumerName = consumer;
            optitons.Wait = 1;
            //optitons.
            try
            {
                OracleAQMessage enqMsg = queue.Dequeue(optitons);
                aq_clob aclob = (aq_clob)enqMsg.Payload;
                return aclob.value;
            }
            catch (OracleException oex)
            {
                const int NO_MESSAGES_ON_QUEUE = 25228;
                if (NO_MESSAGES_ON_QUEUE == oex.Number)
                {
                    return "";
                }
                else throw(oex);
            }

            //return "";


            // Prepare to Enqueue
            //queue.EnqueueOptions.Visibility = OracleAQVisibilityMode.OnCommit;
            //queue.EnqueueOptions.DeliveryMode = OracleAQMessageDeliveryMode.Persistent;
            //enqMsg.Recipients = new OracleAQAgent[]{new OracleAQAgent("SAFO_AG")};
            //enqMsg.Recipients = new OracleAQAgent[] { new OracleAQAgent(recipient) };
            // Enqueue message
            //queue.Enqueue(enqMsg);
            //txn.Commit();

            
        }

        // Function to convert byte[] to string
        public static string ByteArrayToString(byte[] byteArray)
        {
            StringBuilder sb = new StringBuilder();
            for (int n = 0; n < byteArray.Length; n++)
            {
                sb.Append((int.Parse(byteArray[n].ToString())).ToString("X"));
            }
            return sb.ToString();
        }

        public string xml_request(string xml_in)
        {
            OracleCommand cmd = cnn.CreateCommand();

            //cmd.CommandText = "Z$RUNTIME_AQ_LIB.INIT_PUT";
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            string xml_out = "";
            
            cmd.Parameters.Add("xml_in",        OracleDbType.Clob, xml_in,  ParameterDirection.Input);
            cmd.Parameters.Add("xml_out",       OracleDbType.Clob, xml_out, ParameterDirection.Output);
            
            
            string plsqltext = @"declare 
                                  i integer;
                                  p_xml_in clob := :xml_in;                                  
                                begin
                                  i := rtl.open;
                                  :xml_out := Z$EVP_EXT_REQUESTS_L.REQUEST(p_xml_in);
                                  rtl.close(i);                                
                                end;";

            cmd.CommandText = plsqltext;
            cmd.ExecuteNonQuery();

            Oracle.DataAccess.Types.OracleClob c = (Oracle.DataAccess.Types.OracleClob)cmd.Parameters["xml_out"].Value;
            return c.Value.ToString();
        }

        public string xml_request2(string xml_in)
        {
            OracleCommand cmd = cnn.CreateCommand();

            //cmd.CommandText = "Z$RUNTIME_AQ_LIB.INIT_PUT";
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            string xml_out = "";

            cmd.Parameters.Add("xml_in", OracleDbType.Clob, xml_in, ParameterDirection.Input);
            cmd.Parameters.Add("xml_out", OracleDbType.Clob, xml_out, ParameterDirection.Output);


            string plsqltext = @"declare 
                                  i integer;
                                  p_xml_in clob := :xml_in;                                  
                                begin
                                  i := ibs.rtl.open;
                                  :xml_out := ibs.Z$BRK_MSG_L.REQUEST(p_xml_in);
                                  ibs.rtl.close(i);                                
                                end;";

            cmd.CommandText = plsqltext;
            cmd.ExecuteNonQuery();

            Oracle.DataAccess.Types.OracleClob c = (Oracle.DataAccess.Types.OracleClob)cmd.Parameters["xml_out"].Value;
            return c.Value.ToString();
        }

    }
}
